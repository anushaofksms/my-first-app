import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-warning-alert',
  template: `
            <p> This is warning!</p>
            `,
  styles: [`
  p{
    padding:20px;
    color:blue;
    border:1px solid red
  }
  `]
})
export class WarningAlertComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
